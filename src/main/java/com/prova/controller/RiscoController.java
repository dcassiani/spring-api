package com.prova.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prova.entities.Risco;

@RestController
@RequestMapping(value="/riscos")
public class RiscoController {

    @RequestMapping(method=RequestMethod.GET)
    public List<Risco> listRisco() {
        List<Risco> list = new ArrayList<Risco>();
        
        for (Risco r: Risco.values()){
            list.add(r);
        }
        
        return list;
    }
}
