package com.prova.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.prova.entities.Cliente;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "cliente", path = "clientes")
public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    List<Cliente> findByNome(@Param("nome") String nome);
    
}
