package com.prova.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Cliente {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private BigDecimal vrLimiteCredito;

    @Column(nullable = false)
    @JoinColumn(name = "cd_risco")
    private Risco risco;
    
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getVrLimiteCredito() {
        return vrLimiteCredito;
    }

    public void setVrLimiteCredito(BigDecimal vrLimiteCredito) {
        this.vrLimiteCredito = vrLimiteCredito;
    }

    public Risco getRisco() {
        return risco;
    }

    public void setRisco(Risco risco) {
        this.risco = risco;
    }

}
