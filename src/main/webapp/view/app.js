var app = angular.module('app',[]);

app.controller('ClienteController', ['$scope','ClienteService', function ($scope,ClienteService) {
	  
    $scope.updateCliente = function () {
    	if ( ClienteService.validarCliente($scope)  && $scope.cliente.id) {
        	ClienteService.updateCliente($scope.cliente.id, $scope.cliente.nome, $scope.cliente.vrLimiteCredito, $scope.selected.codigo)
        	.then(function success(response){
            	$scope.message = 'Atualizado';
            	$scope.errorMessage = '';
          },
          function error(response){
              	$scope.errorMessage = 'Erro inesperado';
              	$scope.message = '';
          });
        }
    }
    
    $scope.getCliente = function (data) {
    	ClienteService.getCliente(data)
          .then(function success(response){
              $scope.cliente = response.data;
              $scope.message='';
              $scope.errorMessage = '';
              $scope.selected = $scope.cliente.risco;
          },
          function error (response ){
              $scope.message = '';
              if (response.status === 404){
                  $scope.errorMessage = 'Cliente não encontrado';
              } else {
                  $scope.errorMessage = "Erro inesperado - Código não pode ser obtido";
              }
          });
    }
    
    $scope.addCliente = function () {
    	if ( ClienteService.validarCliente($scope) ) {
            ClienteService.addCliente($scope.cliente.nome, $scope.cliente.vrLimiteCredito, $scope.selected.codigo)
              .then (function success(response){
                  $scope.message = 'Inserido';
                  $scope.errorMessage = '';
              },
              function error(response){
                  $scope.errorMessage = 'Erro inserindo cliente';
                  $scope.message = '';
            });
        } else {
            $scope.message = '';
        }
    }
    
    $scope.deleteCliente = function () {
        ClienteService.deleteCliente($scope.cliente.id)
          .then (function success(response){
              $scope.message = 'Cliente Apagado';
              $scope.cliente = null;
              $scope.errorMessage='';
          },
          function error(response){
              $scope.errorMessage = 'Erro inesperado - Código não pode ser obtido';
              $scope.message='';
          })
    }
    
    $scope.listClientes = function () {
         ClienteService.listClientes()
          .then(function success(response){
               $scope.clientes = response.data._embedded.cliente;
              $scope.message='';
              $scope.errorMessage = '';
          },
          function error (response ){
              $scope.message='';
              $scope.errorMessage = 'Erro inesperado';
          });
    }

    ClienteService.listRiscos()
    .then(function success(response){
         $scope.riscos = response.data;
        $scope.message='';
        $scope.errorMessage = '';
        $scope.selected = $scope.riscos[0].codigo;
    },
    function error (response ){
        $scope.message='';
        $scope.errorMessage = 'Erro inesperado';
    });
    
 
    
}]);

app.service('ClienteService',['$http', function ($http) {
	
    this.getCliente = function getCliente(clienteId){
        return $http({
          method: 'GET',
          url: 'clientes/'+clienteId
        });
	}
	
    this.addCliente = function addCliente(nome, vrLimiteCredito, risco){
    	if ((risco == 'B') || (risco=='C')){
	        return $http({
	          method: 'POST',
	          url: 'clientes',
	          data: {nome:nome, risco: risco, vrLimiteCredito:vrLimiteCredito}
	        });
    	} else {
    		$scope.errorMessage = 'Risco obrigatorio';
    		$scope.message = '';
    	}
    }
	
    this.deleteCliente = function deleteCliente(id){
        return $http({
          method: 'DELETE',
          url: 'clientes/'+id
        })
    }
	
    this.updateCliente = function updateCliente(id, nome, vrLimiteCredito, risco){
    	if (risco!='A'){
	        return $http({
	          method: 'PATCH',
	          url: 'clientes/'+id,
	          data: {id:id, nome:nome, risco: risco, vrLimiteCredito:vrLimiteCredito}
	        });
    	} else {
    		return $http({
  	          method: 'PATCH',
  	          url: 'clientes/'+id,
  	          data: {id:id, nome:nome, vrLimiteCredito:vrLimiteCredito}
  	        });
    	}
    }
	
    this.listClientes = function listClientes(){
        return $http({
          method: 'GET',
          url: 'clientes'
        });
    }

    this.listRiscos = function listRiscos(){
        return $http({
          method: 'GET',
          url: 'riscos'
        });
    } 

    this.validarCliente = function validarCliente(data){
    	
    	if (( data.cliente == null ) || ( data.cliente.nome == null) ){
    		data.errorMessage = 'Nome do cliente obrigatorio';
    	} else if (data.cliente.vrLimiteCredito == null ){
    		data.errorMessage = 'Valor de Limite de Crédito inválido';
    	} else if (data.selected.codigo == null){
    		data.errorMessage = 'Risco obrigatorio';
    	} else {
    		return true;
    	}
    	return false;
        
    }
   
}]);

