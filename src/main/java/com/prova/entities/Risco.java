package com.prova.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Risco {

    A("A","manter atual"),
    B("B","10%"),
    C("C","20%");
    
    private String tipo;
    private String codigo;
    
    private Risco(String codigo, String tipo) {
        this.tipo = tipo;
        this.codigo = codigo;
    }
    
    String tipo(){
        return tipo;
    }
    String codigo(){
        return codigo;
    }
    
    public static String porCodigo(String codigo) {
        for (Risco risco: Risco.values()) {
            if (codigo == risco.tipo()) return risco.name();
        }
        throw new IllegalArgumentException("tipo de taxa invalida");
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
}
