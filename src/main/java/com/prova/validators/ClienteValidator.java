package com.prova.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.prova.entities.Cliente;

@Component("clienteValidator")
public class ClienteValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Cliente.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        System.out.println(" ************** A");
        
        Cliente cliente = (Cliente) obj;
        if (validarVazio(cliente.getNome())) {
            errors.rejectValue("nome", "nome.empty");
        }

        if (validarVazio(cliente.getRisco())) {
            errors.rejectValue("risco", "risco.empty");
        }

        if (validarVazio(cliente.getVrLimiteCredito())) {
            errors.rejectValue("vrLimiteCredito", "vrLimiteCredito.empty");
        }

    }

    private boolean validarVazio(Object input) {
        return (input == null || ((String)input).trim().length() == 0);
    }

//    private boolean validarDecimal(String input) {
//        if ((input == null ) || (input.equals(""))) {
//            return false;
//        } else {
//            Pattern pattern = Pattern.compile("^[0-9]*[\\,{1}]?[0-9]*$");
//            Matcher matcher = pattern.matcher(input);
//            boolean isValid = false;
//            while (matcher.find()) {
//                isValid = true;
//            }
//            System.out.println(isValid);
//            return isValid;
//         }
//    }

 
}
