package com.prova.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import com.prova.entities.Cliente;

@Configuration
public class RepoRestConfigurer implements RepositoryRestConfigurer {
    
    public RepoRestConfigurer(){
        super();
    }
        
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
            config.exposeIdsFor(Cliente.class);
    }
}
