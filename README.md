* executar com o comando: mvn spring-boot:run

* navegar para ver a funcionalidade em si: http://localhost:8080/home.html



# melhor sequ�ncia de testes:

* insira valores e clique em "Inserir Novo Cliente (codigo ser� ignorado)"
* repita algumas vezes para abastecer o DB
* clique em "Refresh da Lista de Clientes" para listar o DB
* insira um codigo existente no campo "Codigo Cliente" e clique "Buscar Cliente por Codigo" para carregar um cliente
* ou clique no c�digo diretamenta na lista de clientes para carregar um cliente
* altere os dados mantendo o codigo e clique em "Atualizar Cliente por Codigo" para alterar seus dados
* o tipo A (valor na select em branco) do Risco mantem a taxa original
* clique em "Refresh da Lista de Clientes" para listar o DB
* insira um codigo existente no campo "Codigo Cliente" e clique "Apagar Cliente por Codigo" para apagar um cliente
* clique em "Refresh da Lista de Clientes" para listar o DB


Daniel Cassiani Rezende

(11) 98240-0584

---


Conforme conversamos a respeito do processo seletivo para empresa SRM (www.srmasset.com) para posi��o de Desenvolvedor JAVA. Pe�o a gentileza de realizar o teste em anexo e enviar retorno dentro do prazo de dois dias (caso necessite de mais dias, podemos alinhar).

Per�odo para elabora��o do teste: 02/12 (domingo de noite).

D�vidas, permane�o a disposi��o!

Aguardo confirma��o de recebimento.
Atenciosamente, 

Camila Esquerdo Rossetto

(11) 975273420

Recursos Humanos

www.businesspartners.com.br


---


PREMISSA
Desenvolver camada visual com AngularJS e um servi�o para tratar das regras de neg�cio
Fique � vontade para definir a arquitetura e o modelo de persist�ncia.
- Linguagem: Java
- Front-end: AngularJS
- Inje��o de depend�ncia: Spring
- Informa��es devem ser persistidas em um banco de dados.

FLUXO DE TELA
Criar um formul�rio com os seguintes campos:
- Nome Cliente			Tipo: texto
- Limite de credito		Tipo: num�rico (R$)
- Risco			Tipo: lista		Valores poss�veis: A, B e C

REGRAS DE NEG�CIO
- Todas informa��es devem ser persistidas mais um campo indicando a taxa de juros.
- Todos campos s�o obrigat�rios
- Se o risco for do tipo A manter os dados informados
- Se o risco for do tipo B, a taxa de juros deve ser de 10%
- Se o risco for do tipo C, a taxa de juros deve ser de 20%
